### Notes
---
- Replaces all relevant AceCorp hands with the OG Doomguy hands. Doomguy will also hold short weapons left-handed for consistency with the original pistol and fist in Doom.
- Requires an [AceCorp](https://gitlab.com/accensi/hd-addons) weapon to work. Duh.
- Goes below all AceCorp mods in your load order.

### Supported
---
- Arcanum
- Blackhawk
- Blackjack
- Executioner
- Gungnir
- Hammerhead
- Jackdaw
- Majestic
- Redline
- Scorpion
- Teleporter
- Viper
- Wyvern

### Credits
---
- Accensus: Original Doom-handed sprites for the Blackhawk, Blackjack, Executioner, Gungnir, Majestic, Redline, Scorpion, Teleporter, Viper, and Wyvern.
- Icarus: Original Doom-handed sprites for the Hammerhead.